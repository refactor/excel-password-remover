Author: Stephen Dalton, 2017

Example of removing a password from an Excel spreadsheet using POI. Doesn't work well with big files - but good enough for small stuff.

Code provided as is.