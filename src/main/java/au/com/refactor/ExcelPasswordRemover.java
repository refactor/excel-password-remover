package au.com.refactor;

import org.apache.poi.poifs.crypt.Decryptor;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Created by dalts on 11/01/17.
 */
public class ExcelPasswordRemover {

    public static void main(String[] args) {
        new ExcelPasswordRemover("/tmp/input.xlsx","/tmp/output.xlsx", "<sheet password>" );
    }

    public ExcelPasswordRemover(String inputFilePath, String outputFilePath, String password) {
        System.out.println("Starting...");

        try {
            POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(inputFilePath));
            EncryptionInfo info = new EncryptionInfo(fs);
            Decryptor d = Decryptor.getInstance(info);
            if (d.verifyPassword(password)) {
                XSSFWorkbook wb = new XSSFWorkbook(d.getDataStream(fs));
                FileOutputStream fi = new FileOutputStream(outputFilePath);
                wb.write(fi);
                fi.close();
                System.out.println("Done");
            } else {
                System.out.println("Password incorrect");
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println("File not found");
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            System.err.println("IO Exception");
            ioe.printStackTrace();
        } catch (GeneralSecurityException gse) {
            System.out.println("Security Exception");
            gse.printStackTrace();
        }
    }
}
